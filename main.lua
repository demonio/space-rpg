--Global variables
instance = "load"
if love.filesystem.isFile("client.language") then
  language = require(love.filesystem.read("client.language"))
else
  language = require("text/english")
end
functions = require "functions"
click_sound = functions.generateClickySound()
suit = require 'suit'
images = {}
player = {}
enemy = {}
default_font = love.graphics.newFont( 30 )
default_big = love.graphics.newFont( 50 )
gameinstance = require 'states'
socket = require "socket"
udp = socket.udp()
tcp = socket.tcp()
http = require("socket.http")
server = {}
network = require("network")
optiznost = 0
------------------

-- Local variables
local toggle = false
------------------
--local test = require "test"

function love.load()
  --if love.system.getOS() ~= "Android" then
  --        print("---- RENDERER  ---- ");
  --        local name, version, vendor, device = love.graphics.getRendererInfo()
  --        print(string.format("Name: %s \nVersion: %s \nVendor: %s \nDevice: %s", name, version, vendor, device));
  --end
end

function love.update(dt)
  -- global variables that needs refresh
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  mousex = love.mouse.getX()
  mousey = love.mouse.getY()
  if not gameinstance[instance].first then
      if gameinstance[instance].load then gameinstance[instance].load() end
      gameinstance[instance].first = true
  end
  if gameinstance[instance].first and gameinstance[instance].update then gameinstance[instance].update(dt) end
  love.graphics.setFont(default_font)
end

function love.draw()
  if gameinstance[instance].first and gameinstance[instance].draw then gameinstance[instance].draw() end
  suit.draw()
  --local font = love.graphics.getFont()
  --love.graphics.setFont(default_font)
  love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  --local stats = love.graphics.getStats()

  --local str = string.format("Memory used: %.2f MB", stats.texturememory / 1024 / 1024)
  --love.graphics.print(str, 10, 20)
  --love.graphics.setFont(font)
  -- draw the gui
end

function love.textinput(t)
  if gameinstance[instance].textinput then gameinstance[instance].textinput(dt) end
  -- forward text input to SUIT
  suit.textinput(t)
end

function love.keypressed(key)
  if key == "f11" then
    toggle = not toggle
    love.window.setFullscreen(toggle, "desktop")
  end
  if gameinstance[instance].keypressed then gameinstance[instance].keypressed(key) end
  -- forward keypressed to SUIT
  suit.keypressed(key)
end

function love.mousepressed(x, y, button)
  if gameinstance[instance].mousepressed then gameinstance[instance].mousepressed(x, y, button) end
end
