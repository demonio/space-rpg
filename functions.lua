local functions = {}
function functions.round(num)
    under = math.floor(num)
    upper = math.floor(num) + 1
    underV = -(under - num)
    upperV = upper - num
    if (upperV > underV) then
        return under
    else
        return upper
    end
end
-- generate assets (see love.load)
-- generate sounds
function functions.generateClickySound()
    local snd = love.sound.newSoundData(512, 44100, 16, 1)
    for i = 0,snd:getSampleCount()-1 do
        local t = i / 44100
        local s = i / snd:getSampleCount()
        snd:setSample(i, (.7*(2*love.math.random()-1) + .3*math.sin(t*9000*math.pi)) * (1-s)^1.2 * .3)
    end
    return love.audio.newSource(snd)
end

function functions.generateBox(width,height,color, effect)
  if not effect then
    effect = 0
  end
  if not color then
    color = {255,255,255}
  end
  local alpha = color[4] or 255
  local vertices = {
                {
                        -- top-left corner (red-tinted)
                        0, 3, -- position of the vertex
                        0, 0, -- texture coordinate at the vertex position
                        color[1], color[2], color[3], alpha -- color of the vertex
                },
                {
                        -- top-left corner (red-tinted)
                        3, 0, -- position of the vertex
                        0, 0, -- texture coordinate at the vertex position
                        color[1], color[2], color[3], alpha -- color of the vertex
                },
                {
                        -- top-right corner (green-tinted)
                        width-3, 0,
                        1, 0, -- texture coordinates are in the range of [0, 1]
                        color[1], color[2], color[3], alpha -- color of the vertex
                },
                {
                        -- top-right corner (green-tinted)
                        width, 3,
                        1, 0, -- texture coordinates are in the range of [0, 1]
                        color[1], color[2], color[3], alpha -- color of the vertex
                },
                {
                        -- bottom-right corner (blue-tinted)
                        width, height-3,
                        1, 1,
                        color[1]+effect, color[2]+effect, color[3]+effect, alpha -- color of the vertex
                },
                {
                        -- bottom-right corner (blue-tinted)
                        width-3, height,
                        1, 1,
                        color[1]+effect, color[2]+effect, color[3]+effect, alpha -- color of the vertex
                },
                {
                        -- bottom-left corner (yellow-tinted)
                        3, height,
                        0, 1,
                        color[1]+effect, color[2]+effect, color[3]+effect, alpha -- color of the vertex
                },
                {
                        -- bottom-left corner (yellow-tinted)
                        0, height-3,
                        0, 1,
                        color[1]+effect, color[2]+effect, color[3]+effect, alpha -- color of the vertex
                },
        }
        -- the Mesh DrawMode "fan" works well for 4-vertex Meshes.
        mesh = love.graphics.newMesh(vertices, "fan")
        mesh:setTexture(image)
        return mesh
end
function functions.GetFileExtension(url)
  return url:match("^.+(%..+)$")
end
function functions.GetFileName(url)
  local extension = url:match "[^.]+$"
  local tmp = url:gsub("."..extension, "")
  return tmp
end

function functions.recursiveEnumerate(folder)
        local tabulka = {}
        local lfs = love.filesystem
        local filesTable = lfs.getDirectoryItems(folder)
        for i,v in ipairs(filesTable) do
                local file = folder.."/"..v
                if lfs.isFile(file) then
                        if functions.GetFileExtension(v) == ".png" or
                        functions.GetFileExtension(v) == ".jpg" then
                          tabulka[functions.GetFileName(v)] = love.graphics.newImage(file)
                        else
                          tabulka[functions.GetFileName(v)] = file
                        end
                elseif lfs.isDirectory(file) then
                        tabulka[v] = functions.recursiveEnumerate(file)
                end
        end
        return tabulka
end

function functions.range(from, to, step)
  step = step or 1
  return function(_, lastvalue)
    local nextvalue = lastvalue + step
    if step > 0 and nextvalue <= to or step < 0 and nextvalue >= to or
       step == 0
    then
      return nextvalue
    end
  end, nil, from - step
end

function functions.TableConcat(t1,t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

function functions.random(min,max)
  if max > min then
    return math.random(min,max)
  else
    return min
  end
end

function functions.nameWeapon()
  local nm = {}
  nm[1] = {"Abracadaver","Alakaslam","Alpha","Amnesia","Apostle","Aqua","Ash","Ashes","Ataraxia","Barrage","Benediction","Blackfire","Blackout","Blazefury","Blazewing","Blightspore","Brilliance","Brilliancy","Brimstone","Celeste","Celestia","Chaos","Chaossong","Clarity","Clemence","Cometfall","Consecration","Contortion","Covergence","Crescent","Crucifer","Crucifix","Cryptic","Dawn","Dawne","Dawnlight","Daydream","Deathsong","Deluge","Delusion","Dementia","Doombinder","Doomguard","Doomshadow","Doomward","Dragonbane","Dragonwrath","Draughtbane","Dreambinder","Dreamkiss","Dreamshadow","Dreamsong","Dreamwhisper","Duskshadow","Dusksong","Earthfire","Earthshadow","Earthsong","Ebony","Echo","Eclipse","Emberling","Enigma","Featherfall","Ferallity","Fireweaver","Flameguard","Flamestone","Flameward","Fluke","Flux","Frenzy","Frostguard","Frostward","Fury","Ghost","Glimmer","Harmony","Hubris","Hysteria","Illumina","Inertia","Insanity","Insight","Ivory","Labyrinth","Lament","Lazarus","Lifebender","Lifebinder","Lightbane","Limbo","Lorekeeper","Lull","Lullaby","Malice","Massacre","Mercy","Midnight","Mirage","Moonbeam","Moonlight","Moonshadow","Moonshard","Mystery","Necrolight","Nemesis","Netherbane","Netherlight","Nethersong","Nexus","Nightfall","Nightkiss","Nightmare","Nimble","Nirvana","Nymph","Oathkeeper","Oblivion","Omega","Omen","Oracle","Peacesong","Penance","Persuasion","Phantom","Phantomdream","Phantomlight","Phantomsong","Phobia","Prophecy","Prudence","Purgatory","Pursuit","Quicksilver","Reaper","Reflection","Remorse","Requiem","Retribution","Riddle","Sanguine","Sapience","Scar","Scarlet","Scorchvine","Serenity","Shadowfall","Shadowfeather","Shadowshard","Silence","Silverglow","Silverlight","Sleepwalker","Snowfall","Snowflake","Solarflare","Solarsong","Souleater","Soulflare","Soulkeeper","Soulshadow","Soulshard","Soulsiphon","Soulsliver","Spark","Spellbinder","Spellkeeper","Spellsong","Spire","Splinter","Stardust","Starfall","Starlight","Stormrage","Sunlight","Sunshard","Supinity","Suspension","Thorn","Thunderstrike","Torchlight","Torment","Torrent","Tranquillity","Trinity","Twinkle","Twitch","Valhalla","Verdict","Visage","Void","Whispersong","Windweaver"}
  nm[2] = {"Ancient","Antique","Apocalypse","Apocalyptic","Arcane","Arched","Atuned","Bandit's","Baneful","Banished","Barbarian","Barbaric","Battleworn","Blazefury","Blood Infused","Blood-Forged","Bloodcursed","Bloodied","Bloodlord's","Bloodsurge","Bloodvenom","Bonecarvin","Brutal","Brutality","Burnished","Cataclysm","Cataclysmic","Challenger","Challenger's","Champion","Champion's","Cold-Forged","Conqueror","Conqueror's","Corroded","Corrupted","Crazed","Crying","Cursed","Curved","Dancing","Dark","Darkness","Defender","Defender's","Defiled","Defiling","Deluded","Demonic","Deserted","Desire's","Desolation","Destiny's","Diabolical","Dire","Doom","Doom's","Dragon's","Dragonbreath","Eerie","Enchanted","Engraved","Enlightened","Eternal","Exiled","Extinction","Faith's","Faithful","Fancy","Fearful","Feral","Ferocious","Fierce","Fiery","Fire Infused","Fireguard","Firesoul","Firestorm","Flaming","Flimsy","Forsaken","Fortune's","Foul","Fragile","Frail","Frenzied","Frost","Frozen","Furious","Fusion","Ghastly","Ghost","Ghost-Forged","Ghostly","Gladiator","Gladiator's","Gleaming","Glinting","Greedy","Grieving","Grim","Guard's","Guardian's","Hailstorm","Harmonized","Hateful","Haunted","Heartless","Heinous","Hero","Hero's","Hollow","Holy","Honed","Honor's","Hope's","Hopeless","Howling","Hungering","Improved","Impure","Incarnated","Infused","Inherited","Isolated","Jade Infused","Judgement","Keeper's","Knightly","Knight's","Legionnaire's","Liar's","Lich","Lightning","Lonely","Loyal","Lustful","Lusting","Malevolent","Malicious","Malificent","Malignant","Massive","Mended","Mercenary","Military","Misfortune's","Misty","Moonlit","Mourning","Nightmare","Oathkeeper's","Ominous","Peacekeeper","Peacekeeper's","Phantom","Polished","Possessed","Pride's","Prideful","Primal","Prime","Primitive","Promised","Protector's","Proud","Pure","Putrid","Raging","Recruit's","Refined","Reforged","Reincarnated","Relentless","Remorseful","Renewed","Renovated","Replica","Restored","Retribution","Ritual","Roaring","Ruby Infused","Rune-Forged","Runed","Rusty","Savage","Sentinel","Shadow","Shamanic","Sharpened","Silent","Singed","Singing","Sinister","Skyfall","Smooth","Soldier's","Solitude's","Sorcerer's","Sorrow's","Soul","Soul Infused","Soul-Forged","Soulcursed","Soulless","Spectral","Spectral-Forged","Spiteful","Storm","Storm-Forged","Stormfury","Stormguard","Terror","Thirsting","Thirsty","Thunder","Thunder-Forged","Thunderfury","Thunderguard","Thundersoul","Thunderstorm","Timeworn","Tormented","Trainee's","Treachery's","Twilight","Twilight's","Twisted","Tyrannical","Undead","Unholy","Vanquisher","Vengeance","Vengeful","Venom","Vicious","Victor","Vile","Vindication","Vindicator","Vindictive","Void","Volcanic","Vowed","War","War-Forged","Warden's","Warlord's","Warp","Warped","Warrior","Warrior's","Whistling","Wicked","Wind's","Wind-Forged","Windsong","Woeful","Wrathful","Wretched","Yearning","Zealous"}
  nm[3] = {"Aspect","Bag","Baton","Bauble","Beacon","Bead","Beads","Cage","Cane","Chalice","Charm","Core","Crux","Crystal","Cudgel","Eye","Fan","Focus","Gem","Glaive","Globe","Globule","Goblet","Grail","Harp","Heart","Idol","Insignia","Instrument","Jewel","Juju","Knapsack","Lantern","Orb","Ornament","Paragon","Pouch","Rod","Satchel","Scepter","Scroll","Seal","Skull","Sphere","Stone","Talisman","Torch","Trinket","Urn","Vessel","Visage","Wand"}
  nm[4] = {"of Agony","of Ancient Power","of Anguish","of Ashes","of Assassins","of Black Magic","of Blessed Fortune","of Blessings","of Blight","of Blood","of Bloodlust","of Broken Bones","of Broken Dreams","of Broken Families","of Burdens","of Chaos","of Closing Eyes","of Conquered Worlds","of Corruption","of Cruelty","of Cunning","of Dark Magic","of Dark Souls","of Darkness","of Decay","of Deception","of Degradation","of Delusions","of Denial","of Desecration","of Diligence","of Dismay","of Dragonsouls","of Due Diligence","of Echoes","of Ended Dreams","of Ending Hope","of Ending Misery","of Eternal Bloodlust","of Eternal Damnation","of Eternal Glory","of Eternal Justice","of Eternal Rest","of Eternal Sorrow","of Eternal Struggles","of Eternity","of Executions","of Faded Memories","of Fallen Souls","of Fools","of Frost","of Frozen Hells","of Fury","of Giants","of Giantslaying","of Grace","of Grieving Widows","of Hate","of Hatred","of Hell's Games","of Hellish Torment","of Heroes","of Holy Might","of Honor","of Hope","of Horrid Dreams","of Horrors","of Illuminated Dreams","of Illumination","of Immortality","of Inception","of Infinite Trials","of Insanity","of Invocation","of Justice","of Light's Hope","of Lost Comrades","of Lost Hope","of Lost Voices","of Lost Worlds","of Magic","of Mercy","of Misery","of Mountains","of Mourning","of Mystery","of Necromancy","of Nightmares","of Oblivion","of Perdition","of Phantoms","of Power","of Pride","of Pride's Fall","of Putrefaction","of Reckoning","of Redemption","of Regret","of Riddles","of Secrecy","of Secrets","of Shadow Strikes","of Shadows","of Shifting Sands","of Shifting Worlds","of Silence","of Slaughter","of Souls","of Stealth","of Storms","of Subtlety","of Suffering","of Suffering's End","of Summoning","of Terror","of Thunder","of Time-Lost Memories","of Timeless Battles","of Titans","of Torment","of Traitors","of Trembling Hands","of Trials","of Truth","of Twilight's End","of Twisted Visions","of Unholy Blight","of Unholy Might","of Vengeance","of Visions","of Wasted Time","of Widows","of Wizardry","of Woe","of Wraiths","of Zeal","of the Ancients","of the Banished","of the Basilisk","of the Beast","of the Blessed","of the Breaking Storm","of the Brotherhood","of the Burning Sun","of the Caged Mind","of the Cataclysm","of the Champion","of the Claw","of the Corrupted","of the Covenant","of the Crown","of the Damned","of the Daywalker","of the Dead","of the Depth","of the Dreadlord","of the Earth","of the East","of the Emperor","of the Empty Void","of the End","of the Enigma","of the Fallen","of the Falling Sky","of the Flame","of the Forest","of the Forgotten","of the Forsaken","of the Gladiator","of the Harvest","of the Immortal","of the Incoming Storm","of the Insane","of the King","of the Lasting Night","of the Leviathan","of the Light","of the Lion","of the Lionheart","of the Lone Victor","of the Lone Wolf","of the Lost","of the Moon","of the Moonwalker","of the Night Sky","of the Night","of the Nightstalker","of the North","of the Occult","of the Oracle","of the Phoenix","of the Plague","of the Prince","of the Protector","of the Queen","of the Serpent","of the Setting Sun","of the Shadows","of the Sky","of the South","of the Stars","of the Storm","of the Summoner","of the Sun","of the Sunwalker","of the Talon","of the Undying","of the Victor","of the Void","of the West","of the Whispers","of the Wicked","of the Wind","of the Wolf","of the World","of the Wretched"}

  local first = math.random(1,2)
  local second = math.random(0,2)

  local tmp = ""

  if first > 0 then
    tmp = nm[first][ math.random( 1, #nm[first] - 1 ) ]
  end

  if second > 0 then
    tmp = tmp.." " ..nm[second+2][ math.random( 1, #nm[second+2] - 1 ) ]
  end

  return tmp
end

function functions.tableToText(table)
  local tmp = ""
  for i,v in pairs(table) do
    if type(v) == "table" then
      --tmp = tmp .. functions.tableToText(table)
    else
      tmp = tmp..tostring(i)..":"..tostring(v).."\n"
    end
  end
  return tmp
end

function functions.tableToDict(table)
  local tmp = "{"
  for i,v in pairs(table) do
    if type(v) == "table" then
      --tmp = tmp .. functions.tableToDict(table)
    else
      tmp = tmp.."\'"..tostring(i).."\'"..": "
      if type(v) == "string" then
        tmp = tmp.."\'"..tostring(v).."\'"..","
      else
        tmp = tmp..tostring(v)..","
      end
    end
  end
  tmp = tmp.."}"
  return tmp
end

function functions.tableToValues(table)
  local tmp = ""
  --tmp = "return {"
  for i,v in pairs(table) do
    if type(v) ~= "table" then
      if type(i) == "number" then
        tmp = tmp.."\""..tostring(v).."\",\n"
      else
        tmp = tmp..tostring(i).."= \""..tostring(v).."\",\n"
      end
    else
      if type(i) == "number" then
        tmp = tmp.."{\n"
      else
        tmp = tmp..tostring(i).."= {\n"
      end
      tmp = tmp..tostring(i).."= {\n"
      tmp = tmp..functions.tableToValues(v)
      tmp = tmp.."},\n"
    end
  end
  --tmp = tmp.."}"
  return tmp
end

function functions.recursivelyDelete(item, depth)
        if love.filesystem.isDirectory(item) then
            for _, child in pairs(love.filesystem.getDirectoryItems(item)) do
                functions.recursivelyDelete(item .. '/' .. child, depth + 1);
                love.filesystem.remove(item .. '/' .. child);
            end
        elseif love.filesystem.isFile(item) then
            love.filesystem.remove(item);
        end
        love.filesystem.remove(item)
end

function functions.readItem(table)
  local tmp = ""
  tmp = tmp..language.name..": "..table.name.."\n"
  tmp = tmp..language.dmg..": "..table.dmg.."\n"
  tmp = tmp..language.hp..": "..table.hp.."\n"
  tmp = tmp..language.sp..": "..table.sp .."\n"
  tmp = tmp..language.agi..": "..table.agi .."\n"
  tmp = tmp..language.str..": ".. table.str.."\n"
  tmp = tmp..language.int..": ".. table.int.."\n"
  tmp = tmp..language.speed..": ".. table.speed
  return tmp
end

function functions.readPlayer(table)
  local tmp = ""
  tmp = tmp..language.dmg..": "..table.dmg.."\n"
  tmp = tmp..language.hp..": "..table.hp.."\n"
  tmp = tmp..language.sp..": "..table.sp .."\n"
  tmp = tmp..language.agi..": "..table.agi .."\n"
  tmp = tmp..language.str..": ".. table.str.."\n"
  tmp = tmp..language.int..": ".. table.int.."\n"
  tmp = tmp..language.speed..": ".. table.speed.."\n"
  tmp = tmp..language.exp..": ".. table.exp.."\n"
  tmp = tmp..language.lvl..": ".. table.lvl.."\n"
  tmp = tmp..language.money..": ".. table.money
  return tmp
end

function functions.countPrice(item)
  local tmp = 0
  tmp = (item.dmg*((51-item.speed)/item.speed))/3

  if item.hp then
    --tmp = tmp + ((((item.hp*2)+item.sp)*10)/(250-(((item.int+item.str+item.agi)/3))))*2
    local tmp_2 =  ((item.hp*2+item.sp)/3)
    local tmp_3 = (item.agi+item.str+item.int)/3
    tmp = tmp + (tmp_2*(item.agi/tmp_3))/3
    tmp = tmp + (tmp_2*(item.str/tmp_3))/3
    tmp = tmp + (tmp_2*(item.int/tmp_3))/3
  end

  return functions.round(tmp)
end

function functions.generateEnemy(difficulty)
  local theSeed = os.time()
  for _=1,10 do
    theSeed = theSeed + 1
    --print( "Seeding with "..theSeed )
    math.randomseed( theSeed )
    for i=1,5 do
      math.random( 1, 100 )
    end
  end
  local enemy = {}
  local amount = math.random(1,5)
  local modif = 1

  if difficulty == "hard" then
    modif = 3
  elseif difficulty == "medium" then
    modif = 2
  else
    modif = 1
  end

  local max = 1
  for i,v in pairs(images.enemy[difficulty]) do
    max = max + 1
  end
  math.randomseed(max)
  for i in functions.range(1,amount,1) do
    table = {}
    table.image = images.enemy[difficulty][tostring(math.random(1, max ))]
    local base = ((player.hp + player.sp + player.dmg) * modif/1.5)/(amount*0.75)
    local atributes = ((player.agi + player.str + player.int + player.speed) * modif/1.5)/(amount*0.75)
    tmp = functions.random(100*modif,base*0.35)
		base  = base - tmp
    table.dmg = functions.round(tmp)
    tmp = functions.random(300*modif,base)
		base  = base - tmp
    table.hp = functions.round(tmp)
    table.sp = functions.round(base)
    tmp = functions.random(1,atributes*0.75)
    atributes  = atributes - tmp
    table.agi = functions.round(tmp)
    tmp = functions.random(1,atributes)
		atributes  = atributes - tmp
    table.str = functions.round(tmp)
    tmp = functions.random(1,atributes)
		atributes  = atributes - tmp
    table.int = functions.round(tmp)
    table.speed = functions.random(20,50)
    enemy[i] = table
  end
  return enemy
end

function functions.enemyInfo(table)
  local tmp = ""
  tmp = tmp..language.dmg..": "..table.dmg.."  |  "
  tmp = tmp..language.hp..": "..table.hp.."  |  "
  tmp = tmp..language.sp..": "..table.sp .."  |  "
  tmp = tmp..language.agi..": "..table.agi .."  |  "
  tmp = tmp..language.str..": ".. table.str.."  |  "
  tmp = tmp..language.int..": ".. table.int.."  |  "
  tmp = tmp..language.speed..": ".. table.speed
  return tmp
end

function functions.words(string)
  local words = {}
  for word in string:gmatch("%w+") do table.insert(words, word) end
  return words
end

function functions.saveTable(table)
  tmp = "return {\n"
  tmp = tmp..functions.tableToValues(table)
  tmp = tmp.."}"
  return tmp
end

function functions.isInside(x1,y1,x2,y2,range, range2)
  range2 = range2 or range
  if x1 <= x2+range and x1 >= x2-range and
    y1 <= y2+range2 and y1 >= y2-range2 then
      return true
  end
  return false
end

function functions.isInside2(x1,y1,x2,y2,x2m,y2m)
  if x1 <= x2m and x1 >= x2 and
    y1 <= y2m and y1 >= y2 then
      return true
  end
  return false
end

return functions
