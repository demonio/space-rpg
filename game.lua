local state = {}
state.first = false
local modules = require 'modules.game.init'
local map = require "map"
local entity = require "entity"
local test = nil
local scale
local timer = 0.1
player = {}
enemy = {}
--local player = nil
local tx = 0
local ty = 0
local selected = nil

function state.load()
  test = map.load(player.map)
  planet = test.planet
end

function state.update(dt)
  tx = functions.round(player.x-width/2)
  ty = functions.round(player.y-height/2)
  if timer > 0 then
    timer = timer - dt
  else
    enemy = network.update(player,enemy)
    timer = 0.1
  end
  entity.update(player,dt)
  if selected then
    modules[selected.data.type].active = true
    modules[selected.data.type].loaded = false
    modules[selected.data.type].data = selected
    selected = nil
  end
  --TODO move to server
  --for i,v in pairs(enemy) do
  --  if entity.check(player,v) then
  --    v.hp = v.hp -1
  --    if v.hp == 0 then
  --      table.remove(enemy,i)
  --    end
  --  end
  --end
   if not modules then return end
   for i, v in pairs(modules) do
     if not v.loaded and v.active and v.load then
       v.load()
       v.loaded = true
     end
     if v.active and v.update then
           v.update(dt)
     end
   end
end

function state.draw()
  if not player then return end
  map.draw(test,tx,ty,scale)
  entity.draw(player,test,tx,ty,scale)
  for i,v in pairs(enemy) do
    entity.draw(v,test,tx,ty,scale)
  end
  if not modules then return end
  for i, v in pairs(modules) do
    if v.active and v.draw then
      v.draw()
    end
  end
end

function state.textinput(t)
    if not modules then return end
    for i, v in pairs(modules) do
      if v.active and v.textinput then
        v.textinput(t)
      end
    end
end

function state.keypressed(key)
    if not modules then do return end end
    for i, v in pairs(modules) do
      if v.active and v.keypressed then
        if v.keypressed(key) then return end
      end
    end
    if key == "space" then
      entity.shoot(player)
    end
end

function state.mousepressed(x, y, button)
    if not modules then do return end end
    for i, v in pairs(modules) do
      if v.active and v.mousepressed then
        if v.mousepressed(x, y, button) then return end
      end
    end
    if button == 1 then
      entity.shoot(player)
    end
    if button == 2 then
      selected = map.object_check(x,y,test,tx,ty)
    end
end

return state
