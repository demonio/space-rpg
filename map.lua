local map = {}

function map.load(name)
	local tmp = require ('maps/'..name)
	for i,v in pairs(tmp.tilesets) do
		v.imagedata = love.graphics.newImage(v.image)
		local tmp_id = v.firstgid
		tmp.tilesets[i].quad = {}
		for y in functions.range(0,v.imageheight-(v.tileheight+v.spacing),v.tileheight+v.spacing) do
			for x in functions.range(0,v.imagewidth-(v.tilewidth+v.spacing),v.tilewidth+v.spacing) do
					tmp.tilesets[i].quad[tmp_id] = love.graphics.newQuad(x, y, v.tilewidth, v.tileheight, v.imagewidth,v.imageheight)
					tmp_id=tmp_id+1
			end
		end
		v.lastgrid = tmp_id
	end

	-- generate map so we can detect better
	for i,v in ipairs(tmp.layers) do
		v.map = {}
		for x in functions.range(1,v.width,1) do
			v.map[x] = {}
			for y in functions.range(1,v.height,1) do
				v.map[x][y] = v.data[x+((y-1)*v.width)]
			end
		end
	end

	--TODO move to server
	tmp.planet = {}
	local planet_count = math.random(1, 12)
	for i in functions.range(1,planet_count,1) do
		tmp.planet[i] = {}
		tmp.planet[i].x = math.random(64,192)*256
		tmp.planet[i].y = math.random(64,192)*256
		tmp.planet[i].image = love.graphics.newImage("images/planet/"..math.random(1,4)..".png")
		tmp.planet[i].data = {
			id = i,
			type = "planet",
			shop = math.random(0,1),
			mission = math.random(0,1),
			dock = math.random(0,1)
		}
	end
	return tmp
end

function map.addLayer(tmp,compare)
	-- TODO dont use
	tl = tmp.layers[compare]
	tmp.layers[#tmp.layers+1] = {
		x = tl.x,
		y = tl.y,
		width = tl.width,
		height = tl.height,
	}
end

function map.object_check(x,y,tmp,tx,ty)
	local ty = ty or 0
	local tx = tx or 0
	for i,v in pairs(tmp.planet) do
		if functions.isInside(x+tx,y+ty,v.x,v.y,v.image:getWidth()/2,v.image:getHeight()/2) then
			return v
		end
	end
	return nil
end

function map.test(x,y,tmp,layer)
	if not tmp.layers[layer].map[x] then return nil end
	return tmp.layers[layer].map[x][y] or nil
end

function map.draw(tmp,tx,ty,scale)
	local mtx = (tx or 0)+love.graphics.getWidth()
	local mty = (ty or 0)+love.graphics.getHeight()
	local ty = ty or 0
	local tx = tx or 0
	local scale = scale or 1
  for i,v in ipairs(tmp.layers) do
		if v.type == "tilelayer" then
			for k,z in pairs(tmp.tilesets) do
					local tmp_id = 1
					local y_min = functions.round(ty/(z.tileheight*scale))
					y_min = (y_min > 1 and y_min-1 or 1)
					local y_max = functions.round(mty/(z.tileheight*scale))
					y_max = (y_max < v.height and y_max+1 or v.height)
					for y in functions.range(y_min,y_max,1) do
						local x_min = functions.round(tx/(z.tilewidth*scale))
						x_min = (x_min > 1 and x_min-1 or 1)
						local x_max = functions.round(mtx/(z.tilewidth*scale))
						x_max = (x_max < v.width and x_max+1 or v.width)
						for x in functions.range(x_min,x_max,1) do
							tmp_id = x+((y-1)*v.width)
							--print(x_min,x_max,y_min,y_max,tmp_id)
							if (z.firstgid <= v.data[tmp_id] or v.data[tmp_id] == 0) and z.lastgrid > v.data[tmp_id] then
								if v.data[tmp_id] > 0 then
									love.graphics.draw(z.imagedata,
		 						 				tmp.tilesets[k].quad[v.data[tmp_id]],
		 						 				(((x-1)*z.tilewidth)*scale)-tx,
		 										(((y-1)*z.tileheight)*scale)-ty,
		 										0, scale, scale,
		 										v.offsetx,
		 										v.offsety)
								end

							end
						end
				end
			end
		end
	end
	for i,v in pairs(tmp.planet) do
		if functions.isInside2(v.x,v.y,tx,ty,mtx,mty) then
			love.graphics.draw(v.image, v.x-tx,v.y-ty, 0, scale, scale,v.image:getWidth()/2,v.image:getHeight()/2)
		end
	end
end

return map
