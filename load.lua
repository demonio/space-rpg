local state = {}
state.first = false
local ready = false
local load = false
local skip = true

function state.load()

end

function state.update(dt)
  if skip then
    skip = not skip
    return
  end
  if not load then
    --suit.theme.color = {
    --  normal  = {bg = { 238, 238, 238,100}, fg = {33,33,33}},
    --  hovered = {bg = { 232,106, 23,100}, fg = {33,33,33}},
    --  active  = {bg = { 30, 167, 225,100}, fg = {33,33,33}}
    --}
    network.load ()
    images = functions.recursiveEnumerate("images")
    love.filesystem.createDirectory("score")
    load = true
  end
  if love.timer.getFPS() > 25 then
	ready = true
  end
  if ready and load then
    instance = "login"
  end
end

function state.draw()
  if not ready then
    suit.Label(language.loading, width/2-150,height/2-15, 300,30)
  end
end

return state
