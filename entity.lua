local entity = {}
local map = require "map"

function entity.load(where, name)
  local tmp = {}
  tmp.x = 123*256
	tmp.y = 123*256
	tmp.speed = 0
	tmp.max_speed = 500
	tmp.deacceleration = 250
	tmp.acceleration = 125
	tmp.rotation = 0
  tmp.hp = 20
  tmp.hp_max = 20
  tmp.sp = 10
  tmp.sp_max = 10
  tmp.regen = 1
  tmp.shoot = {}
  tmp.timer = 0
  tmp.race = 1
  tmp.inventory_size = 20
  tmp.inventory = {}
  tmp.money = 0
  tmp.image = love.graphics.newImage("images/Enemies/enemyRed1.png")

  return tmp
end

function entity.update(what,dt)
  if love.keyboard.isDown("left") then
		what.rotation = what.rotation - (math.pi*dt*what.speed/what.max_speed)
	end
	if love.keyboard.isDown("right") then
		what.rotation = what.rotation + (math.pi*dt*what.speed/what.max_speed)
	end
	if love.keyboard.isDown("up") then
		what.speed = (what.speed < what.max_speed and what.speed + what.acceleration*dt or what.max_speed)
	end
	if love.keyboard.isDown("down") then
    if what.speed > 0 then
		    what.speed = (what.speed > 0 and what.speed - what.deacceleration*dt or what.speed - what.acceleration*dt*0.5)
    else
      what.speed = 0
    end
	end
	what.x=what.x+(math.cos(what.rotation-1.5708) * what.speed * dt)
	what.y=what.y+(math.sin(what.rotation-1.5708) * what.speed * dt)
	if what.speed < -what.max_speed/2 then
		what.speed = -what.max_speed/2
	end
  for i,v in ipairs(what.shoot) do
    if v.timer > 0 then
      v.timer = v.timer-dt
      v.x=v.x+(math.cos(v.angle-1.5708) * v.speed * dt)
    	v.y=v.y+(math.sin(v.angle-1.5708) * v.speed * dt)
    else
      table.remove(what.shoot,i)
    end
  end
end

function entity.get(what)
  return network.getEntity(what)
end

function entity.check(what, hit)
  for i,v in ipairs(what.shoot) do
    if functions.isInside(v.x,v.y,hit.x,hit.y,hit.image:getWidth()) then
      table.remove(what.shoot,i)
      return true
    end
  end
  return false
end

function entity.shoot(what,x,y)
  local shot = {}
  shot.x = what.x
  shot.y = what.y
  shot.speed = 1000
  shot.timer = 3

  if not x then
    shot.angle = what.rotation
  else
    shot.dx = what.x + x
    shot.dy = what.y + y

    shot.angle = math.atan2((shot.dy - shot.y), (shot.dx - shot.x))
  end


  table.insert(what.shoot, shot)
end

function entity.draw(what,where,tx,ty,scale)
  local mtx = (tx or 0)+love.graphics.getWidth()
  local mty = (ty or 0)+love.graphics.getHeight()
  local ty = ty or 0
  local tx = tx or 0
  if functions.isInside2(what.x,what.y,tx,ty,mtx,mty) then
    local image = images["ships"][tostring(what.image)]
    love.graphics.draw(image,what.x-tx,what.y-ty,what.rotation,scale,scale,image:getWidth()/2,image:getHeight()/2)
    for i,v in ipairs(what.shoot) do
      --love.graphics.draw(v.)
      love.graphics.circle("fill", v.x-tx, v.y-ty, 5)
      --print(v.x-tx, v.y-ty)
    end
  end
end

return entity
