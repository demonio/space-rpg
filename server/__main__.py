# -*- coding: utf-8 -*-
#!/usr/bin/python

# -----------------------------------------------------------------
# imports

import os
import SocketServer
import sys
import ast
import json
from Queue import *
clients = {}
test = 0

def use(data):
    global clients
    tmp = data.split()
    if tmp[0] == "register":
        clients[tmp[1]] = client(tmp[1])
        if clients[tmp[1]].register(tmp[2]):
            return "DONE"
    if tmp[0] == "login":
        if clients[tmp[1]].login(tmp[2]):
            return "DONE"
    if tmp[0] == "entity":
        return globals()[tmp[0]+"_"+tmp[1]](tmp[2])
    return "ERROR"

def send_all(name):
    msg = ""
    done = False
    for key,value in clients.iteritems():
        if key != name and value.map == clients[name].map:
            msg += entity_get(key)+","
            done = True
    if done:
        msg = "{"+msg+"}"
    return msg



def entity_get(name):
    tmp_user = clients[name]
    tmp_user.password = "\"" + tmp_user.password + "\""
    tmp_user.name = "\"" + tmp_user.name + "\""
    tmp = str(vars(tmp_user))
    tmp = tmp.replace(":","=")
    tmp = tmp.replace("'","")
    tmp_user.password = tmp_user.password.replace("\"","")
    tmp_user.name = tmp_user.name.replace("\"","")
    return tmp

class client():

    def __init__(self, name):
        self.map = 1
        self.name = name
        self.password = ""
        self.x = 123 * 256
        self.y = 123 * 256
        self.speed = 0
        self.max_speed = 500
        self.deacceleration = 250
        self.acceleration = 125
        self.rotation = 0
        self.hp = 20
        self.hp_max = 20
        self.sp = 10
        self.sp_max = 10
        self.regen = 1
        self.shoot = {}
        self.timer = 0
        self.race = 1
        self.inventory_size = 20
        self.inventory = dict()
        self.money = 0
        self.image = 1

    def register(self, password):
        #add register test
        self.password = password
        return True

    def login(self, password):
        #print(self.password)
        if self.password == password:
            return True
        return False

    def __getitem__(self,item):
        return self.__dict__[item]

    def __setitem__(self,item,value):
        self.__dict__[item] = value

    def update(self,data):
        #self = ast.literal_eval(data)
        tmp = ast.literal_eval(data)
        for key,value in tmp.iteritems():
            self[key] = value

class galaxie():

    def __init__(self):
        self.planets = dict()
        self.gates = dict()
        self.asteroids = dict()
        self.npc = dict()

class chat():

    def __init__(self):
        self.line = ""

class EchoRequestHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        # Echo the back to the client
        data = self.request.recv(1024).strip()
        print(data)
        if data.find("Connection test") > -1:
            self.request.send(data.upper())
        else:
            self.request.send(use(data))
        return

class UDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        socket = self.request[1]
        tmp = data.split()
        if tmp[0] == "update":
            clients[tmp[1]].update(str(data.replace(tmp[0]+" "+tmp[1]+" ",""))[:-2]+"}")
            socket.sendto(send_all(tmp[1]), self.client_address)



if __name__ == '__main__':
    import threading
    address = ('localhost', 25000) # let the kernel give us a port
    address2 = ('localhost', 25001) # let the kernel give us a port
    server = SocketServer.TCPServer(address, EchoRequestHandler)
    server2 = SocketServer.UDPServer(address2, UDPHandler)

    t = threading.Thread(target=server.serve_forever)
    t.setDaemon(True) # don't hang on exit
    t.start()
    t2 = threading.Thread(target=server2.serve_forever)
    t2.setDaemon(True)
    t2.start()
    #server.serve_forever()
    print 'Server loop running in process:', os.getpid()
    while 1:
        try:
            # DO THINGS
            command = raw_input(">")
        except KeyboardInterrupt:
            # quit
            server.socket.close()
            server2.socket.close()
            sys.exit()
