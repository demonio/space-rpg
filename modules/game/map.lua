local state = {}
state.loaded = false
state.active = true
local timer = 1
local channel = "global"
local message = { text = "" }
local chat = {}
local font = love.graphics.getFont()
local slider = {value = 1, min = 1, max = 10}
local bg_bg = functions.generateBox(276, 276, {125,125,125,75}, -75)
local bg = functions.generateBox(266, 266, {200,200,200,75}, -75)
local map = functions.generateBox(256, 256, {0,0,0,75}, 0)
state.visible = true
state.x = 0
state.y = 0
state.width = 276
state.height = 276

function state.load()
end

function state.keypressed(key)

end

function state.update(dt)

end

function state.mousepressed(x, y, button)
  if x > state.x and x < state.x+state.width and
    y > state.y and y < state.y+state.height and state.visible then
      return true
  end
  return false
end

function state.draw()
  if state.visible then
    love.graphics.draw(bg_bg, width-state.width,0)
    love.graphics.draw(bg, width-state.width+5,5)
    love.graphics.draw(map, width-state.width+10,10)
    love.graphics.setPointSize(3)
    k =functions.round(player.x/256)
    i =functions.round(player.y/256)
    love.graphics.setColor(0,255,0)
    love.graphics.points((width-state.width+10)+k, 10+i)
    love.graphics.setColor(255,255,255)
    if planet then
      for n,m in pairs(planet) do
        k = functions.round(m.x/256)
        i = functions.round(m.y/256)
        love.graphics.setColor(0,0,255)
        love.graphics.points((width-state.width+10)+k, 10+i)
        love.graphics.setColor(255,255,255)
      end
    end
    for n,m in pairs(enemy) do
      k = functions.round(m.x/256)
      i = functions.round(m.y/256)
      love.graphics.setColor(255,0,0)
      love.graphics.points((width-state.width+10)+k, 10+i)
      love.graphics.setColor(255,255,255)
    end
    love.graphics.setPointSize(1)
  end
end

return state
