local state = {}
state.loaded = false
state.active = true
local timer = 1
local channel = "global"
local message = { text = "" }
local chat = {}
local font = love.graphics.getFont()
local slider = {value = 1, min = 1, max = 10}
local bg_bg = functions.generateBox(430, 260, {125,125,125,75}, -75)
local bg = functions.generateBox(420, 250, {200,200,200,75}, -75)
state.visible = false
state.x = 0
state.y = 0
state.width = 430
state.height = 260

function state.load()
  love.keyboard.setKeyRepeat( true )
end

function state.keypressed(key)
  if key == "c" and not state.visible then
    state.visible = not state.visible
    return true
  end
  if key == "escape" and state.visible then
    state.visible = not state.visible
    return true
  end
  return false
end

function state.update(dt)
  state.x = 0
  state.y = height-260
  if state.visible then
    if timer > 0 then
      timer = timer - dt
    else
      --chat = network.get("chat", "GET "..player.name)
      timer = 1
    end

    suit.Input(message, 10,height-40, 250,30)
    --suit.Label(chat,{align="left"}, 10,height-240, 420)
    if suit.Button(language.send, 270,height-40, 150,30).hit and string.len(message.text) > 0 then
      network.send("chat",channel.." "..player.name.." "..message.text)
      if message.text:sub(1,1) == "*" then
        --TODO command
        local tmp = message.text:gsub("*","")
        tmp = functions.words(tmp)
        --TODO write commands
      else
        --TODO send
      end
      message = { text = "" }
    end
    if chat.line then
      if chat.line > 10 then
        slider.max = chat.line-10
        suit.Slider(slider,{vertical = true}, 400,height-240, 20,180)
      end
    end
  end
end

function state.mousepressed(x, y, button)
  if x > state.x and x < state.x+state.width and
    y > state.y and y < state.y+state.height and state.visible then
      return true
  end
  return false
end

function state.draw()
  if state.visible then
    love.graphics.draw(bg_bg, 0,height-260)
    love.graphics.draw(bg, 5,height-255)
    love.graphics.setColor(0, 0, 0)
    if chat.line then
      local lines = 1
      for i in functions.range(functions.round(slider.value),chat.line-1, 1) do
        local size = font:getWidth(chat.text[i])
        if size > 380 then
          lines = lines + 2
        else
          lines = lines + 1
        end
        if lines > 15 then
          break
        end
        love.graphics.printf( chat.text[i], 10, (height-50)-font:getHeight()*(lines-1), 380, "left" )

      end
    end
    love.graphics.setColor(255, 255, 255)
  end
end

return state
