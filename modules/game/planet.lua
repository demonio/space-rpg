local state = {}
state.loaded = false
state.active = false
local timer = 1
local channel = "global"
local message = { text = "" }
local chat = {}
local font = love.graphics.getFont()
local slider = {value = 1, min = 1, max = 10}
local bg_bg = functions.generateBox(430, 430, {125,125,125,75}, -75)
local bg = functions.generateBox(420, 420, {200,200,200,75}, -75)
state.visible = false
state.x = 0
state.y = 0
state.width = 430
state.height = 430
state.data = nil

function state.load()
  state.x = width/2-state.width/2
  state.y = height/2-state.height/2
end

function state.keypressed(key)
  if key == "escape" then
    state.active = false
    return true
  end
  return false
end

function state.update(dt)
  if not functions.isInside(player.x,
                            player.y,
                            state.data.x,
                            state.data.y,
                            state.data.image:getWidth(),
                            state.data.image:getHeight()) then
    state.active = false
  end
  local data = state.data.data
  --TODO rewrite
  if data.shop then
    suit.Button("shop",state.x+10,state.y+70, state.width-20,50)
  end
  if data.mission then
    suit.Button("mission",state.x+10,state.y+130, state.width-20,50)
  end
  if data.dock then
    suit.Button("dock",state.x+10,state.y+190, state.width-20,50)
  end
  if suit.Button(language.close,state.x+10,state.y+190, state.width-20,50).hit then
    state.active = false
  end
end

function state.mousepressed(x, y, button)

end

function state.draw()
  love.graphics.draw(bg_bg,state.x,state.y)
  love.graphics.draw(bg,state.x+5,state.y+5)
end

return state
