local state = {}
state.loaded = false
state.active = true
local timer = 1
local channel = "global"
local message = { text = "" }
local chat = {}
local font = love.graphics.getFont()
local slider = {value = 1, min = 1, max = 10}
local bg_bg = functions.generateBox(300, 130, {125,125,125,75}, -75)
local bg = functions.generateBox(290, 120, {200,200,200,75}, -75)
state.visible = true
state.x = 0
state.y = 0
state.width = 430
state.height = 130

function state.load()
end

function state.keypressed(key)

end

function state.update(dt)

end

function state.mousepressed(x, y, button)
  if x > state.x and x < state.x+state.width and
    y > state.y and y < state.y+state.height and state.visible then
      return true
  end
  return false
end

function state.draw()
  if state.visible then
    love.graphics.draw(bg_bg, 0,0)
    love.graphics.draw(bg, 5,5)
  end
end

return state
