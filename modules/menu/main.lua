local state = {}
state.loaded = false
state.active = true
local go = nil
local bg_bg = functions.generateBox(320, 370, {50,50,50,50}, 0)
local bg = functions.generateBox(310, 360, {200,200,200,50}, -75)
local credits = love.graphics.newImage("credits.png")

function state.load()

end

function state.update(dt)
  --love.graphics.setFont(menuf)
  if suit.Button(language.play,width/2-150,(height/2)-70*2,300,60).hit then
    instance = "game"
  end
  if suit.Button(language.score,width/2-150,(height/2)-70,300,60).hit then
    instance = "score"
  end
  if suit.Button(language.options,width/2-150,(height/2),300,60).hit then
    go = "options"
  end
  if suit.Button(language.quit,width/2-150,(height/2)+70*2,300,60).hit then
    love.event.quit()
  end
end

function state.open()
  if go then
    local tmp = go
    go = nil
    state.active = false
    return tmp
  end
  return go
end

function state.keypressed(key)
  if key == "escape" then
    love.event.quit()
  end
end

function state.draw()
  love.graphics.draw(bg_bg, width/2-160,((height/2)-70*2)-10)
  love.graphics.draw(bg, width/2-155,((height/2)-70*2)-5)
  love.graphics.draw(credits, width-credits:getWidth()-10,height-credits:getHeight()-10)
end

return state
