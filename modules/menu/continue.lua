local state = {}
state.loaded = false
state.active = true
local go = nil

function state.load()

end

function state.update(dt)

end

function state.open()
  if go then
    local tmp = go
    go = nil
    return tmp
  end
  return go
end

function state.draw()

end

return state
