local state = {}
state.loaded = false
state.active = true
local tmp = 0
local button = {}
local selected = 0
local hovered = 0
local bg_bg = functions.generateBox(300, 350, {125,125,125}, -75)
local bg = functions.generateBox(290, 340, {200,200,200}, -75)
local bg_item = functions.generateBox(80, 80, {200,75,75}, -75)
local go = nil

function state.load()
  if tonumber(player.skin) == 0 then
    state.active = false
  end
  if tonumber(player.skill_1) ~= 0 then
    state.active = false
  end
end

function state.update(dt)
  suit.Label(language.choose_weapon, (width/2-2.5*80)+(80*(0-1)),((height/2-40)+(80*(0-3))), 320,30)
  local c = 1
  local k = 1
  for i,v in pairs(images.weapons.common) do
      button[i] = {}
      button[i].width = ((width/2+300)-2.5*80)+(80*(k-1))
      button[i].height =  ((height/2-70)+(80*(c-3)))
      button[i].image = v
      button[i].data = suit.ImageButton(v,button[i].width,button[i].height)
      --button[i].info =
      if button[i].data.hit then
        selected = tonumber(i)
        button[i].info = network.get("weapons", "GET "..i)
      end
      if button[i].data.hovered then
        hovered = tonumber(i)
      end
      if k < 5 then
        k = k +1
      else
        k = 1
        c = c+1
      end
  end
  if selected > 0 then
    local submit = suit.Button(language.submit, 205,70,175,30)
    local close = suit.Button(language.close, 205,110,175,30)
    if submit.hit then
      network.player("update", player.name.." skill_1 "..selected)
      go = "modes"
    end
    if close.hit then
      selected = 0
      selected = 0
    end
  end
end

function state.open()
  if go then
    local tmp = go
    go = nil
    state.active = false
    return tmp
  end
  return go
end

function state.draw()
  if selected > 0 then
    love.graphics.draw(bg_bg, 105,55)
    love.graphics.draw(bg, 110,60)
    love.graphics.draw(button[tostring(selected)].image, 120,70)
  end
  if hovered > 0 then
    local k = tostring(hovered)
    love.graphics.draw(bg_item, button[k].width-5, button[k].height-5)
  end
end

return state
