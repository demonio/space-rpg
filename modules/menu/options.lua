local state = {}
state.loaded = false
state.active = false
local width_bg = 195
local height_bg = 200
local x = 50
local y = 50
local go = nil
local select = ""
local bg_bg = functions.generateBox(width_bg, height_bg, {75,75,125}, -75)
local bg = functions.generateBox(width_bg-10, height_bg-10, {75,75,200}, -75)

function state.load()
  x = width/2 - width_bg/2
  y = height/2 - height_bg/2
end

function state.update(dt)
  --love.graphics.setFont(gamef)
  if select == "" then
    if suit.Button(language.language, x+width_bg-185,y+10,175,30).hit then
      select = language.language
    end
  elseif select == language.language then
    local tmp = 1
    for i,v in pairs(functions.recursiveEnumerate("text")) do
      if suit.Button(i, x+width_bg-185,(y+10)+35*tmp,175,30).hit then
        language = require(string.gsub(v, functions.GetFileExtension(v), ""))
        if love.filesystem.isFile("client.language") then
          love.filesystem.write("client.language", functions.GetFileName(v))
        else
          love.filesystem.newFile("client.language")
          love.filesystem.write("client.language", functions.GetFileName(v))
        end
        select = ""
      end
      tmp = tmp + 1
    end
  end
  local close = suit.Button(language.close, x+width_bg-185,y+height_bg-40,175,30)
  if close.hit then
    go = "main"
  end
  if love.mouse.isDown(2) then
    local mouse_x = love.mouse.getX()
    local mouse_y = love.mouse.getY()
    if mouse_x > x and mouse_x < x+width_bg and
      mouse_y > y and mouse_y < y+height_bg then
        x = mouse_x-width_bg/2
        y = mouse_y-height_bg/2
    end
  end
end

function state.keypressed(key)
  if key == "escape" then
    go = "main"
  end
end

function state.open()
  if go then
    local tmp = go
    go = nil
    state.active = false
    return tmp
  end
  return go
end

function state.draw()
  love.graphics.draw(bg_bg, x,y)
  love.graphics.draw(bg, x+5,y+5)
end

return state
