local state = {}
state.first = false
local modules = require 'modules.menu.init'

function state.load()

end

function state.update(dt)
   if not modules then return end
   for i, v in pairs(modules) do
     if not v.loaded and v.active and v.load then
       v.load()
       v.loaded = true
     end
     if v.active and v.update then
           v.update(dt)
     end
     if v.active and v.open then
       local tmp = v.open()
       if tmp then
        modules[tmp].active = not modules[tmp].active
      end
     end
   end
end

function state.draw()
  --love.graphics.draw(images.background.winter, 0, 0,0,width/1920, height/1080)
  if not modules then return end
  for i, v in pairs(modules) do
    if v.active and v.draw then
      v.draw()
    end
  end
end

function state.textinput(t)
    if not modules then return end
    for i, v in pairs(modules) do
      if v.active and v.textinput then
        v.textinput(t)
      end
    end
end

function state.keypressed(key)
    if not modules then do return end end
    for i, v in pairs(modules) do
      if v.active and v.keypressed then
        v.keypressed(key)
        if key == "escape" and not v.active then
          break
        end
      end
    end
end

function state.mousepressed(x, y, button)
    if not modules then do return end end
    for i, v in pairs(modules) do
      if v.active and v.mousepressed then
        v.mousepressed(x, y, button)
      end
    end
end

return state
