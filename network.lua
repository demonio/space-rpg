local network = {}

function network.load ()
    if love.filesystem.isFile("server.address") then
        server.address = love.filesystem.read("server.address")
    else
        server.address = "localhost"
        love.filesystem.newFile("server.address")
        love.filesystem.write("server.address", server.address)
    end
    if love.filesystem.isFile("server.tcp") then
        server.tcp = love.filesystem.read("server.tcp")
        server.udp = love.filesystem.read("server.udp")
    else
        server.tcp = 25000
        server.udp = 25001
        love.filesystem.newFile("server.tcp")
        love.filesystem.write("server.tcp", server.tcp)
        love.filesystem.newFile("server.udp")
        love.filesystem.write("server.udp", server.tcp)
    end
    udp = socket.udp()
    udp:settimeout(0)
    udp:setpeername("localhost",25001)
end

function network.connect(port)
    tcp = socket.tcp()
    tcp:connect(server.address, port);
end

function network.close ()
    tcp:close()
end

function network.test()
    network.connect(server.tcp)
    --note the newline below
    tcp:send("Connection test\n");
    tcp:settimeout(1)
    local timer = 10
    done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        if partial == "CONNECTION TEST" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.getEntity(name)
    network.connect(server.tcp)
    --note the newline below
    tcp:send("entity get "..name.."\n");
    tcp:settimeout(1)
    local timer = 10
    done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial then
            done = loadstring("return "..partial)()
            timer = 0
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.update(tmp,tmp2)
  udp:send("update "..tmp.name.." "..functions.tableToDict(tmp))
  data, msg = udp:receive()
  tmp2 = {}
  if data then
    --print(data)
    tmp2 = loadstring("return "..data)()
  end
  return tmp2
end

function network.login (name, password)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("login "..name.." "..password.."\n");
    tcp:settimeout(1)
    local timer = 10
    done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        if partial == "DONE" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.register (name, password)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("register "..name.." "..password.."\n");
    tcp:settimeout(1)
    local timer = 10
    done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial == "DONE" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.get(where, what)
      network.connect (server.tcp)
      tcp:send(where.." "..what)
      timer = 60
      done = {}
      done.text = {}
      done.line = 1
      error = true
      while timer > 0 do
          local s, status, partial = tcp:receive()
          if s then
            done.text[done.line] = s
            done.line = done.line + 1
          end
          if partial == "RECIVED" then
              timer = 0
              error = false
              break
          end
          timer = timer - 1
      end
      if error then
        done = language.server_error
      end
      network.close ()
      return done
  end


function network.send(where, what)

    network.connect (server.tcp)
    tcp:send(where.." "..what)
    timer = 60
    done = false
    while timer > 0 do
        local s, status, partial = tcp:receive()
        if partial == "RECIVED" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

return network
