local state = {}
state.first = false
local button = {}
local username = { text = language.username }
local password = { text = language.password }
local status = "OFFLINE"
local timer = 0
local scale = 1
local bg_bg = functions.generateBox(450, 280, {255,75,75,100}, 0)
local bg = functions.generateBox(440, 270, {255,75,75,100}, -75)
local registered = false

function state.load()
  if love.filesystem.isFile("player.username") then
    username = { text = love.filesystem.read("player.username") }
    password = { text = love.filesystem.read("player.password") }
 end
 if network.test() then
   status = "ONLINE"
 end
 love.keyboard.setKeyRepeat(true)
end

function state.update(dt)
  suit.Label(language.server_status..status, width/2-160,height/2-130, 320,50)
  button.username = suit.Input(username, width/2-205,height/2-70, 410,50)
  button.password = suit.Input(password, width/2-205,height/2-10, 410,50)
  button.login = suit.Button(language.login, width/2-205,height/2+50, 200,50)
  button.register = suit.Button(language.register, width/2+5,height/2+50, 200,50)
  if button.register.hit then
    if network.register (username.text, password.text) then
      timer = 300
      registered = true
    end
  end
  if button.login.hit then
    if network.login (username.text, password.text) then
      player = network.getEntity(username.text)
      print(type(player))
      print(player.map)
      if not love.filesystem.isFile("player.username") then
          love.filesystem.newFile("player.username")
          love.filesystem.write("player.username", username.text)
          love.filesystem.newFile("player.password")
          love.filesystem.write("player.password", password.text)
      else
          love.filesystem.write("player.username", username.text)
          love.filesystem.write("player.password", password.text)
      end
      love.keyboard.setKeyRepeat(false)
      instance = "game"
    else
      timer = 300
    end
  end
  if timer > 0 then
    if registered then
      suit.Label(language.register_done, width/2-160,height/2+135, 320,50)
    else
      suit.Label(language.login_error, width/2-160,height/2+135, 320,50)
    end
    timer = timer - 1
  end
  if timer == 0 then
    registered = false
  end
  if width < height then
    scale = width/1920
  else
    scale = height/1080
  end
end

function state.draw()
  --love.graphics.draw(images.background.grass, 0, 0,0,width/1920, height/1080)
  love.graphics.draw(bg_bg, width/2-225,height/2-140)
  love.graphics.draw(bg, width/2-220,height/2-135)
end

return state
